from pathlib import Path

from fastapi.testclient import TestClient

from src.api.main import app

client = TestClient(app)


def test_convert_xls_pdf():
    test_xls_path = Path("tests/data/test_1.xlsx")
    response = client.post(
        "/convert",
        files=dict(input_file=test_xls_path.open("rb")),
        data=dict(output_type="pdf"),
    )
    assert response.content.startswith(b"%PDF-1.7")
