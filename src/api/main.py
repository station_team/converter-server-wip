import secrets
from pathlib import Path

from fastapi import BackgroundTasks
from fastapi import Body
from fastapi import FastAPI
from fastapi import File
from fastapi import Request
from fastapi import UploadFile
from fastapi.responses import FileResponse
from fastapi.responses import JSONResponse

from src import core

TEMP = Path("temp").resolve()

app = FastAPI()


@app.exception_handler(NotImplementedError)
async def not_implemented_error_handler(request: Request, exc: NotImplementedError):
    return JSONResponse(
        status_code=501,
        content={"message": "Feature not implemented"},
    )


@app.exception_handler(Exception)
async def unexpected_error_handler(request: Request, exc: Exception):
    return JSONResponse(
        status_code=500,
        content={"message": "Unexpected error"},
    )


def remove_path(path: Path):
    if path.is_file() or path.is_symlink():
        path.unlink()
        return
    for p in path.iterdir():
        remove_path(p)
    path.rmdir()


@app.post("/convert")
def convert(
    tasks: BackgroundTasks,
    input_file: UploadFile = File(...),
    output_type: core.Format = Body(..., description="Desired file format", embed=True),
):
    temp_workdir = TEMP / secrets.token_urlsafe(16)
    temp_workdir.mkdir()
    tasks.add_task(remove_path, temp_workdir)

    local_input_fp = temp_workdir / input_file.filename

    with local_input_fp.open("wb") as f:
        f.write(input_file.file.read())

    converter = core.get_converter(core.File(local_input_fp))
    output_fp = converter.convert(output_type)
    return FileResponse(str(output_fp))
