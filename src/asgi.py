from uvicorn import run

if __name__ == "__main__":
    run("api.main:app", port=80, reload=True)
