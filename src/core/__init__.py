from src.core.converters import excel
from src.core.converters.base import BaseConverter
from src.core.models import File
from src.core.models import Format


def get_converter(file: File) -> BaseConverter:
    if file.format in [Format.XLS, Format.XLSX]:
        return excel.ExcelConverter(file)
    raise NotImplementedError()
