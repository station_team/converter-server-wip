import time
from contextlib import contextmanager
from pathlib import Path
from typing import Optional
from typing import Union

import pythoncom
from win32com import client as win32com_client
from win32com.universal import com_error as win32com_error

from src.core.converters.base import BaseConverter


@contextmanager
def dispatch_excel():
    pythoncom.CoInitialize()
    application = win32com_client.DispatchEx("Excel.Application")
    application.DisplayAlerts = False
    try:
        yield application
    except TypeError:
        application.Quit()
    except Exception as e:
        raise e
    finally:
        application.DisplayAlerts = True
        application.Quit()


@contextmanager
def open_workbook(application, fp: Path, save: bool = False):
    excel_suffixes = [".xls", ".xlsx"]
    if fp.suffix not in excel_suffixes:
        raise Exception(f"Wrong extension: {fp.suffix}. Must be on of {excel_suffixes}")
    workbook = application.Workbooks.Open(fp)
    time.sleep(5)
    try:
        yield workbook
    except win32com_error:
        workbook.Close(SaveChanges=False)
    except Exception as e:
        raise e
    finally:
        workbook.Close(SaveChanges=save)


class ExcelConverter(BaseConverter):
    def convert_to_pdf(
        self,
        out_filepath: Optional[Path] = None,
        sheet: Union[int, str] = 0,
        landscape: bool = False,
    ) -> Path:
        if not self.file.path:
            raise ValueError("Only filepath is supported.")
        if not out_filepath:
            out_filepath = self.file.path.with_suffix(".pdf")
        with dispatch_excel() as app:
            with open_workbook(app, self.file.path) as workbook:
                workbook.RefreshAll()
                workbook.Save()
                if sheet:
                    # Save only one sheet
                    worksheet = workbook.Worksheets(sheet)
                    worksheet.Visible = 1
                    worksheet.PageSetup.Zoom = False
                    worksheet.PageSetup.FitToPagesWide = 1
                    worksheet.PageSetup.FitToPagesTall = 999
                    worksheet.PageSetup.Orientation = 2 if landscape else 1
                    worksheet.SaveAs(str(out_filepath), FileFormat=57)
                else:
                    # Save all sheets
                    workbook.ExportAsFixedFormat(0, str(out_filepath))
        return out_filepath

    def convert_to_xls(self) -> Path:
        raise NotImplementedError()
