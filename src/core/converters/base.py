from pathlib import Path

from src.core.models import File
from src.core.models import Format


class BaseConverter:
    def __init__(self, file: File):
        self.file = file

    def convert(self, out_format: Format, **kwargs) -> Path:
        if out_format is Format.PDF:
            return self.convert_to_pdf(**kwargs)
        if out_format in [Format.XLS, Format.XLSX]:
            return self.convert_to_xls(**kwargs)
        raise NotImplementedError()

    def convert_to_pdf(self, *args, **kwargs) -> Path:
        raise NotImplementedError()

    def convert_to_xls(self, *args, **kwargs) -> Path:
        raise NotImplementedError()
