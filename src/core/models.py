from enum import Enum
from pathlib import Path
from typing import Optional


class Format(Enum):
    XLS = "xls"
    XLSX = "xlsx"
    PDF = "pdf"


class File:
    def __init__(
        self,
        path: Optional[Path] = None,
        content: Optional[bytes] = None,
        file_format: Optional[Format] = None,
        file_name: Optional[str] = None,
    ):
        if not any([path, all([content, file_format, file_name])]):
            raise ValueError()

        if path:
            self.path = path.resolve()
            self.name = path.stem
            self.format = Format(path.suffix.strip("."))
        else:
            self.path = None
            self.format = file_format

        if file_name:
            self.name = file_name

        if content:
            self.content = content
        else:
            with self.path.open(mode="rb") as f:
                self.content = f.read()
