# Converter server

Simple FastAPI server that allows converting files. Only excel -> pdf conversion supported by now.

# Setup

Install requirements:

```shell
poetry install
```

Set PYTHONPATH:

```shell
# pwsh
$Env:PYTHONPATH = "."

# bash
PYTHONPATH="." 
```

Run:

```shell
python "src/asgi.py"
```
